import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 05.06.18
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 */
public class BaseTest {

    static WebDriver driver = initChromedriver();

    @Before
    public void navigateToLoginPage() {
        driver.get("https://wallet.stage.trueplay.io/login.html");
    }

    @AfterClass
    public static void cleanup() {
        driver.quit();
    }

    @Test
    public void Test1() {
        Reg();
        WebElement email = driver.findElement(By.id("regForm:emailreg"));
        email.sendKeys("joras@ukr.net");
        WebElement phoneName = driver.findElement(By.id("regForm:phonereg"));
        phoneName.sendKeys("380663836253");
        WebElement password = driver.findElement(By.name("regForm:passwreg"));
        password.sendKeys("1234567");
        WebElement passwordRepeat = driver.findElement(By.name("regForm:repasswreg"));
        passwordRepeat.sendKeys("1234567");
        Button();
        WebElement container = driver.findElement(By.id("message_container"));
        String containerText = container.getText();
        assertEquals("Captcha: Validation Error: Value is required.", containerText);
        System.out.println("Captcha: Validation Error: Value is required.");
    }

    private void Reg() {
        WebElement reg = driver.findElement(By.id("reg"));
        reg.click();
    }


    @Test
    public void Test2() {
        Reg();
        Button();
        WebElement messageContainer = driver.findElement(By.cssSelector("#regForm > div.emls > span"));
        String messageContainerText = messageContainer.getText();
        assertEquals("Email должен соответствовать формату a@a.a", messageContainerText);
        System.out.println("Email должен соответствовать формату a@a.a");

    }

    private void Button() {
        WebElement button2 = driver.findElement(By.cssSelector("#regForm\\3a signupBtn > span"));
        button2.click();
    }

    @Test
    public void Test3() {
        Reg();
        Button();
        WebElement Conteiner = driver.findElement(By.cssSelector("#regForm > div.tells > span"));
        String ConteinerText = Conteiner.getText();
        assertEquals("Неверный формат", ConteinerText);
        System.out.println("Неверный формат");
    }
    @Test
    public void Test4() {
        Reg();
        Button();
        WebElement element = driver.findElement(By.cssSelector("#regForm > div.passw > span.passwerr1"));
        String ConteinerText = element.getText();
        assertEquals("Должно быть не менее 6 символов", ConteinerText);
        System.out.println("Должно быть не менее 6 символов");
    }
    @Test
    public void Test5() {
        Reg();
        Button();
        WebElement webElement = driver.findElement(By.cssSelector("#regForm > div.repassw > span"));
        String ConteinerText = webElement.getText();
        assertEquals("Подтверждение не совпадает с паролем", ConteinerText);
        System.out.println("Подтверждение не совпадает с паролем");
    }
    @Test
    @Ignore
    public void Test6() {
        Reg();
        WebElement email = driver.findElement(By.id("regForm:emailreg"));
        email.sendKeys("joras@ukr.net");
        WebElement phoneName = driver.findElement(By.id("regForm:phonereg"));
        phoneName.sendKeys("380663836253");
        WebElement password = driver.findElement(By.name("regForm:passwreg"));
        password.sendKeys("1234567");
        WebElement passwordRepeat = driver.findElement(By.name("regForm:repasswreg"));
        passwordRepeat.sendKeys("1234567");
        WebElement button3 = driver.findElement(By.xpath("//*[@id=\"recaptcha-anchor\"]/div[5]"));
        button3.click();
    }

    public static WebDriver initChromedriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver();
    }

}
